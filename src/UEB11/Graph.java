package ueb11;

import java.util.*;

class Graph {
	private static final int KNOTENZAHL = 6;
	private static boolean[] besucht;
	private KnotenTyp[] knoten;
	private boolean[][] matrix = { { false, true, false, false, false, false },
			{ false, false, true, true, false, false },
			{ false, true, false, true, false, false },
			{ false, true, true, false, false, false },
			{ false, false, false, false, true, false },
			{ false, false, false, false, false, true }, };

	Graph() 
		{
		knoten = new KnotenTyp[KNOTENZAHL];
		knoten[0] = new KnotenTyp("A");
		knoten[1] = new KnotenTyp("B");
		knoten[2] = new KnotenTyp("C");
		knoten[3] = new KnotenTyp("D");
		knoten[4] = new KnotenTyp("E");
		knoten[5] = new KnotenTyp("F");
		}

	boolean isKante(int k1, int k2) 
		{
		// Ueberpruefen der Eintraege in Adjazenzmatrix fuer beide Richtungen
		return (matrix[k1][k2] || matrix[k2][k1]);
		}

	void Zusammenhangskomponenten() 
		{
		String ausgabe = "";
		besucht = new boolean[KNOTENZAHL];
		//Hilfsarray: true, wenn f�r den Knoten die Nachbarn bestimmt wurden.
		boolean[] neighboursChecked = new boolean[KNOTENZAHL];
		int loopCounter;
		for (int i = 0; i < KNOTENZAHL; i++) 
			{
			//Knoten bestimmen, die noch nicht besucht wurden
			if (!besucht[i]) 
				{
				besucht[i] = true;
				ausgabe += knoten[i].getName() + " ";
				
				//Beginnend mit dem ersten Knoten der jeweiligen ZHK werden die Nachbarn der besuchten Knoten durchsucht
				//Erst wenn alle besuchten Nachbarn ge-checkt sind endet die Schleife
				loopCounter = i;
				while (!Arrays.equals(besucht, neighboursChecked)) 
					{
					for (int j = 0; j < KNOTENZAHL; j++)	
						{
						if ( (!besucht[j]) && (isKante(loopCounter, j))) 
							{
							besucht[j] = true;
							ausgabe += knoten[j].getName() + " ";
							}
						}
					neighboursChecked[loopCounter] = true;
					loopCounter += 1;
					//loopCounter beginnt wieder von vorn, Knoten k�nnen Nachbarn mit kleinerem Index haben
					loopCounter = (loopCounter >= KNOTENZAHL) ? 0 : loopCounter;
					}
				//ZHK ist hiermit bestimmt
				System.out.println(ausgabe);
				ausgabe = "";
				}
			}
		}

	public static void main(String[] args) 
		{
		new Graph().Zusammenhangskomponenten();
		}
}
