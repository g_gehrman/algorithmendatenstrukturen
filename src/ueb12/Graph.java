package ueb12;

class Graph
	{
	//Hilfskonstante  um Double-Werte auf "Gleichheit" zu �berpr�fen
	static final double EPSILON = 0.00001;
	private static double[][] matrix1 =
		{
		{0.0, 5.0, 0.0, 0.0, 0.0, 0.0},
		{5.0, 0.0, 5.0, 5.0, 0.0, 0.0},
		{0.0, 5.0, 0.0, 5.0, 0.0, 0.0},
		{0.0, 5.0, 5.0, 0.0, 0.0, 0.0},
		{0.0, 0.0, 0.0, 0.0, 5.0, 0.0},
		{0.0, 0.0, 0.0, 0.0, 0.0, 5.0},
		};

	private static double[][] matrix2 =
		{
		{0.0, 5.0, 0.0, 0.0, 0.0, 0.0},
		{0.0, 0.0, 5.0, 5.0, 0.0, 0.0},
		{0.0, 5.0, 0.0, 5.0, 0.0, 0.0},
		{0.0, 5.0, 5.0, 0.0, 0.0, 0.0},
		{0.0, 0.0, 0.0, 0.0, 5.0, 0.0},
		{0.0, 0.0, 0.0, 0.0, 0.0, 5.0},
		};
	
	//mit Knoten 1 anfangen: F�r jede Kante von 1 �berpr�fen, ob die Eintr�ge f�r beide Richtungen identisch sind
	//Wenn ein Knoten gefunden wird, wo diese Bedingung nicht zutrifft: false
	private static boolean isSymmetric(double[][] matrix)
		{
		assert(matrix!=null);
		assert(matrix[0]!=null);
		assert(matrix.length==matrix[0].length);
		
		for (int i = 0; i < matrix.length-1; i++)
			for(int j = i+1; j < matrix.length; j++)
				if (!isEdgeSymmetric(matrix,i,j))
					return false;
		return true;
		}
	
	//Hilfsmethode zum �berpr�fen von Gleichheit einer Kante in beide Richtungen
	private static boolean isEdgeSymmetric(double[][] matrix, int k1, int k2)
		{
		return (Math.abs(matrix[k1][k2] - matrix[k2][k1]) < EPSILON);
		}

	//Die erste Kante, die ungleich 0 ist gibt die Gewichtung vor, die alle anderen Knoten ebenfalls haben m�ssen, damit
	//der Graph trival gewichtet ist
	private static boolean isTriviallyWeighted(double[][] matrix)
		{
		assert(matrix!=null);
		assert(matrix[0]!=null);
		assert(matrix.length==matrix[0].length);
		
		double edgeWeight = 0;
		double weightDiff = 0;
		for(int i = 0; i < matrix.length; i++)
			for (int j = 0; j < matrix.length; j++)
				{
				weightDiff = Math.abs(matrix[i][j] - edgeWeight);
				//edgeWeight initialisieren
				if (matrix[i][j] > 0 && edgeWeight == 0)
					edgeWeight = matrix[i][j];
				
				else if ( (matrix[i][j] > 0) && (edgeWeight > 0) && (weightDiff > EPSILON) )
					return false;
				}
		return true;
		}

	public static void main(String[] args)
		{
		System.out.println("isSymmetric: " + isSymmetric(matrix1) + ", " + isSymmetric(matrix2));
		System.out.println("isTriviallyWeighted: " + isTriviallyWeighted(matrix1) + ", " + isTriviallyWeighted(matrix2));
		}
	}

